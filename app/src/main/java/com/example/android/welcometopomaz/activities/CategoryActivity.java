package com.example.android.welcometopomaz.activities;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.android.welcometopomaz.adapters.CategoryAdapter;
import com.example.android.welcometopomaz.objects.Attraction;
import com.example.android.welcometopomaz.R;

import java.util.ArrayList;

/**
 * CategoryActivity displays a list of attractions in the clicked category.
 */

public class CategoryActivity extends AppCompatActivity {

    // Attributes of the attractions: titles, descriptions, pictures and contacts
    String[] attractionTitles;
    String[] attractionDescriptions;
    TypedArray attractionPictureIds;
    String[] attractionContacts;
    // Resources
    Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        // Adding an 'up' button to the ActionBar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        // Get the category name from the intent
        String category = getIntent().getStringExtra("CATEGORY");
        // Modify the ActionBar title
        setTitle(category);
        res = getResources();
        setupAttractionAttributes(category);
        // Create a RecycleView, LayoutManager and Adapter for displaying the attractions list
        RecyclerView recyclerView = findViewById(R.id.attractions_list);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        CategoryAdapter adapter = new CategoryAdapter(createAttractionList(defineLength()));
        recyclerView.setAdapter(adapter);
        // Recycle TypedArray
        if (attractionPictureIds != null) {
            attractionPictureIds.recycle();
        }
    }

    /**
     * Create the attribute lists for the CategoryActivity to display, based on the
     *
     * @param category
     */
    private void setupAttractionAttributes(String category) {
        // Get the menu item texts from the resources
        final String[] categoryNames = res.getStringArray(R.array.category_names);
        final String[] cateringSubmenuNames = res.getStringArray(R.array.catering_submenu_list);
        final String[] surroundingsSubmenuNames = res.getStringArray(R.array.surroundings_submenu_list);
        // Create the correct lists for the CategoryActivity.
        if (category.equals(categoryNames[0])) {
            attractionPictureIds = res.obtainTypedArray(R.array.sights_picture_ids);
            attractionTitles = res.getStringArray(R.array.sight_titles);
            attractionDescriptions = res.getStringArray(R.array.sight_descriptions);
        } else if (category.equals(categoryNames[1])) {
            attractionTitles = res.getStringArray(R.array.event_titles);
            attractionDescriptions = res.getStringArray(R.array.event_descriptions);
        } else if (category.equals(categoryNames[2])) {
            attractionTitles = res.getStringArray(R.array.accommodation_titles);
            attractionDescriptions = res.getStringArray(R.array.accommodation_descriptions);
            attractionContacts = res.getStringArray(R.array.accommodation_contacts);
        } else if (category.equals(categoryNames[5])) {
            attractionTitles = res.getStringArray(R.array.transportation_titles);
            attractionDescriptions = res.getStringArray(R.array.transportation_descriptions);
            attractionContacts = res.getStringArray(R.array.transportation_contacts);
        } else if (category.equals(categoryNames[6])) {
            attractionTitles = res.getStringArray(R.array.emergency_number_names);
            attractionContacts = res.getStringArray(R.array.emergency_number_contacts);
        } else if (category.equals(cateringSubmenuNames[0])) {
            attractionTitles = res.getStringArray(R.array.restaurant_titles);
            attractionDescriptions = res.getStringArray(R.array.restaurant_description);
            attractionContacts = res.getStringArray(R.array.restaurant_contacts);
        } else if (category.equals(cateringSubmenuNames[1])) {
            attractionTitles = res.getStringArray(R.array.patisserie_titles);
            attractionDescriptions = res.getStringArray(R.array.patisserie_description);
            attractionContacts = res.getStringArray(R.array.patisserie_contacts);
        } else if (category.equals(cateringSubmenuNames[2])) {
            attractionTitles = res.getStringArray(R.array.pub_titles);
            attractionDescriptions = res.getStringArray(R.array.pub_description);
            attractionContacts = res.getStringArray(R.array.pub_contacts);
        } else if (category.equals(surroundingsSubmenuNames[0])) {
            attractionPictureIds = res.obtainTypedArray(R.array.settlements_picture_ids);
            attractionTitles = res.getStringArray(R.array.settlement_titles);
            attractionDescriptions = res.getStringArray(R.array.settlement_descriptions);
        } else if (category.equals(surroundingsSubmenuNames[1])) {
            attractionTitles = res.getStringArray(R.array.tourist_destination_names);
            attractionPictureIds = res.obtainTypedArray(R.array.tourist_destination_picture_ids);
        } else {
            throw new RuntimeException("There is not a valid list to display");
        }
    }

    /**
     * @return the length of the attractionList to create based on the attribute lists.
     */
    private int defineLength() {
        if (attractionTitles != null) {
            return attractionTitles.length;
        } else if (attractionDescriptions != null){
            return attractionDescriptions.length;
        } else if (attractionContacts != null) {
            return attractionContacts.length;
        } else {
            return attractionPictureIds.length();
        }
    }

    /**
     * @return a list of attractions based on the attribute lists
     */
    @NonNull
    private ArrayList<Attraction> createAttractionList(int length) {
        ArrayList<Attraction> attractionList = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            Attraction item = new Attraction();
            if (attractionTitles != null) {
                item.setAttractionTitle(attractionTitles[i]);
            }
            if (attractionDescriptions != null) {
                item.setDescription(attractionDescriptions[i]);
            }
            if (attractionPictureIds != null) {
                item.setImageResourceId(attractionPictureIds.getResourceId(i, -1));
            }
            attractionList.add(item);
            if (attractionContacts != null) {
                item.setContacts(attractionContacts[i]);
            }
        }
        return attractionList;
    }
}
