package com.example.android.welcometopomaz.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.android.welcometopomaz.R;
import com.example.android.welcometopomaz.adapters.MenuAdapter;
import com.example.android.welcometopomaz.objects.MenuListItem;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A small list of clickable categories
 */

public class MenuActivity extends AppCompatActivity {

    private Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        // Disable the 'Up' button in the ActionBar
        getSupportActionBar().setHomeButtonEnabled(false);      // Disable the button
        getSupportActionBar().setDisplayHomeAsUpEnabled(false); // Remove the left caret
        getSupportActionBar().setDisplayShowHomeEnabled(false); // Remove the icon
        // Get the Resources
        res = getResources();
        // Set the title of the ActionBar
        setTitle(res.getString(R.string.app_name));
        configRecyclerView(createMenuList());
    }

    /**
     * Create a list of MenuListItem objects for the MenuAdapter
     *
     * @return the list of MenuListItems
     */
    private ArrayList<MenuListItem> createMenuList() {
        String[] categoryNames = res.getStringArray(R.array.category_names);
        TypedArray categoryPictureIds = res.obtainTypedArray(R.array.category_picture_ids);
        ArrayList<MenuListItem> menuList = new ArrayList<>();
        for (int i = 0; i < categoryNames.length; i++) {
            // Get the image resource id
            int imageResourceId = categoryPictureIds.getResourceId(i, -1);
            // Check if there isn't any
            if (imageResourceId == -1) {
                throw new RuntimeException("There is a not valid Picture ID in the main menu");
            }
            // Create the MenuListItems with or without the submenu
            MenuListItem object;
            switch (categoryNames[i]) {
                case "Catering":
                    ArrayList<String> submenu1 = new ArrayList<>(Arrays.asList(res
                            .getStringArray(R.array.catering_submenu_list)));
                    object = new MenuListItem(categoryNames[i], imageResourceId, submenu1);
                    break;
                case "Surroundings":
                    ArrayList<String> submenu2 = new ArrayList<>(Arrays.asList(res
                            .getStringArray(R.array.surroundings_submenu_list)));
                    object = new MenuListItem(categoryNames[i], imageResourceId, submenu2);
                    break;
                default:
                    // Create the MenuListItem object without the submenu
                    object = new MenuListItem(categoryNames[i], imageResourceId);
                    break;
            }
            // Add the new object to the menuList
            menuList.add(object);
        }
        // Recycle the TypedArray
        categoryPictureIds.recycle();
        return menuList;
    }

    /**
     * Configure the RecyclerView to display a list of menu items.
     */
    private void configRecyclerView(ArrayList<MenuListItem> menuList) {
        // Create a RecycleView, LayoutManager and Adapter for displaying the menu items
        RecyclerView categoryList = findViewById(R.id.categories_list);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        categoryList.setLayoutManager(manager);
        // Create a divider between the list items
        DividerItemDecoration divider =
                new DividerItemDecoration(categoryList.getContext(), manager.getOrientation());
        categoryList.addItemDecoration(divider);
        MenuAdapter adapter = new MenuAdapter(menuList);
        categoryList.setAdapter(adapter);
    }

    // Variables for double back press
    private long lastPressedTime;
    private static final int TIME_INTERVAL = 2000;

    /**
     * If the back button pressed within 2000 millisecond, exit the application.
     * Else there is a Toast message shown.
     */
    @Override
    public void onBackPressed() {
        if (lastPressedTime + TIME_INTERVAL > System.currentTimeMillis()) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(startMain);
            finish();
        } else {
            Toast.makeText(this, "Double tap for exit", Toast.LENGTH_SHORT).show();
            lastPressedTime = System.currentTimeMillis();
        }
    }
}