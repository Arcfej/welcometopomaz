package com.example.android.welcometopomaz.activities;

import android.content.Intent;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.android.welcometopomaz.R;

/**
 * This is the launcher class of the app. It's just a welcome screen.
 */

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Hide the ActionBar
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.hide();
        }
        setContentView(R.layout.activity_welcome);
        // Crop the ImageView's bottom
        ImageView image = findViewById(R.id.welcome_image);
        Matrix matrix = image.getImageMatrix();
        float imageWidth = image.getDrawable().getIntrinsicWidth();
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        float scaleRatio = screenWidth / imageWidth;
        matrix.postScale(scaleRatio, scaleRatio);
        image.setImageMatrix(matrix);
        // Start the MenuActivity with the categories
        View root = findViewById(R.id.welcome_screen);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent categoryActivity = new Intent(WelcomeActivity.this, MenuActivity.class);
                startActivity(categoryActivity);
            }
        });
    }
}