package com.example.android.welcometopomaz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.welcometopomaz.activities.CategoryActivity;
import com.example.android.welcometopomaz.R;

import java.util.ArrayList;

/**
 * SubmenuAdapter displays a list of items in a RecyclerView in the MenuActivity.
 * The items have a text and an OnClickListener.
 */

public class SubmenuAdapter extends RecyclerView.Adapter<SubmenuAdapter.SubcategoryHolder> {

    // The list of the texts to display
    private ArrayList<String> mSubmenuList;

    public SubmenuAdapter(ArrayList<String> submenuList) {
        mSubmenuList = submenuList;
    }

    /**
     * The custom ViewHolder of the SubmenuAdapter
     */
    public class SubcategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // TextView of the submenu item
        private TextView mTextView;

        public SubcategoryHolder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.textView);
            itemView.setOnClickListener(this);
        }

        /**
         * Opens the CategoryActivity and send the name of the category to it.
         */
        @Override
        public void onClick(View view) {
            Context context = itemView.getContext();
            Intent categoryListIntent = new Intent(context, CategoryActivity.class);
            categoryListIntent.putExtra("CATEGORY", mSubmenuList.get(getAdapterPosition()));
            context.startActivity(categoryListIntent);
        }
    }

    /**
     * Inflate the simple_list_item for the SubMenu
     *
     * @param parent   of the item
     * @param viewType is not used
     * @return the newly created SubcategoryHolder
     */
    @Override
    public SubcategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_list_item, parent, false);
        return new SubcategoryHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(SubcategoryHolder holder, int position) {
        holder.mTextView.setText(mSubmenuList.get(position));
    }

    @Override
    public int getItemCount() {
        return mSubmenuList.size();
    }
}