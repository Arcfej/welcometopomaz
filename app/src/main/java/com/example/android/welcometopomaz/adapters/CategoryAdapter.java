package com.example.android.welcometopomaz.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.welcometopomaz.objects.Attraction;
import com.example.android.welcometopomaz.R;
import com.example.android.welcometopomaz.views.SquareImageView;

import java.util.ArrayList;

/**
 * This adapter displays a list of attractions in a RecyclerView in the CategoryActivity.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.AttractionHolder> {

    private ArrayList<Attraction> mAttractionList;

    /**
     * @param attractionList is a list of object to display in the RecyclerView
     */
    public CategoryAdapter(ArrayList<Attraction> attractionList) {
        mAttractionList = attractionList;
    }

    /**
     * Custom ViewHolder for the CategoryAdapter
     */
    public static class AttractionHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private TextView mDescription;
        private SquareImageView mImage;
        private TextView mContacts;
        // The holder (parent) of the mContacts. It's needed for the visibility changes.
        private View mContactsHolder;

        /**
         * Set the resources of the ViewHolder
         */
        public AttractionHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.attraction_title);
            mDescription = itemView.findViewById(R.id.attraction_description);
            mImage = itemView.findViewById(R.id.attraction_image);
            mContacts = itemView.findViewById(R.id.attraction_contacts);
            mContactsHolder = itemView.findViewById(R.id.attraction_contacts_holder);
        }
    }

    /**
     * @param parent   of the ViewHolder.
     * @param viewType isn't used.
     * @return a new AttractionHolder
     */
    @Override
    public AttractionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View textView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.details_item, parent, false);
        return new AttractionHolder(textView);
    }

    /**
     * Set the views visible if they have content to display
     *
     * @param holder   of the views
     * @param position of the holder
     */
    @Override
    public void onBindViewHolder(AttractionHolder holder, int position) {
        Attraction item = mAttractionList.get(position);
        String title = item.getAttractionTitle();
        String description = item.getDescription();
        String contacts = item.getContacts();
        int imageResourceId = item.getImageResourceId();
        if (title != null) {
            holder.mTitle.setText(title);
            holder.mTitle.setVisibility(View.VISIBLE);
        }
        if (description != null) {
            holder.mDescription.setText(description);
            holder.mDescription.setVisibility(View.VISIBLE);
        }
        if (imageResourceId != -1) {
            holder.mImage.setImageResource(imageResourceId);
            holder.mImage.setVisibility(View.VISIBLE);
        }
        if (contacts != null) {
            holder.mContacts.setText(contacts);
            holder.mContactsHolder.setVisibility(View.VISIBLE);
        }
    }

    /**
     * @return the number of items in the adapter
     */
    @Override
    public int getItemCount() {
        return mAttractionList.size();
    }
}