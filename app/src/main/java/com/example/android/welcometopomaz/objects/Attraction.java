package com.example.android.welcometopomaz.objects;

/**
 * An Attraction object holds the attributes of a list item
 */

public class Attraction {

    private String mAttractionTitle = null;
    private String mDescription = null;
    private int mImageResourceId = -1;
    private String mContacts = null;

    public String getAttractionTitle() {
        return mAttractionTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getImageResourceId() {
        return mImageResourceId;
    }

    public String getContacts() {
        return mContacts;
    }

    public void setAttractionTitle(String attractionTitle) {
        mAttractionTitle = attractionTitle;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setImageResourceId(int imageResourceId) {
        mImageResourceId = imageResourceId;
    }

    public void setContacts(String contacts) {
        mContacts = contacts;
    }
}
