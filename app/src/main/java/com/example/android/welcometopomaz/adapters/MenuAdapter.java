package com.example.android.welcometopomaz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.welcometopomaz.R;
import com.example.android.welcometopomaz.activities.CategoryActivity;
import com.example.android.welcometopomaz.objects.MenuListItem;

import java.util.ArrayList;

/**
 * MenuAdapter displays a list of items in a RecyclerView in the MenuActivity.
 * The items have an image, a text and an OnClickListener, and may or may not have a submenu.
 */

public class MenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MenuAdapter";
    // List of MenuListItem objects
    private ArrayList<MenuListItem> mMenuList;

    /**
     * @param menuList is the list of objects to display.
     */
    public MenuAdapter(ArrayList<MenuListItem> menuList) {
        mMenuList = menuList;
    }

    /**
     * ViewHolder for menu_list_items (menu items without submenu)
     */
    public class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //The Image- and TextView of the list item
        ImageView mItemImage;
        TextView mItemText;

        public CategoryHolder(View view) {
            super(view);
            mItemImage = view.findViewById(R.id.category_image);
            mItemText = view.findViewById(R.id.category_text);
            view.setOnClickListener(this);
        }

        /**
         * Opens the CategoryActivity and send the name of the category to it.
         */
        @Override
        public void onClick(View view) {
            Context context = itemView.getContext();
            Intent categoryListIntent = new Intent(view.getContext(), CategoryActivity.class);
            categoryListIntent
                    .putExtra("CATEGORY", mMenuList.get(getAdapterPosition()).getText());
            context.startActivity(categoryListIntent);
        }
    }

    /**
     * ViewHolder for submenu_list_items (menu items with submenu)
     */
    public class SubmenuHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //The Image- and TextView of the list item
        ImageView mItemImage;
        TextView mItemText;
        // The submenu
        RecyclerView mSubmenu;

        public SubmenuHolder(View view) {
            super(view);
            mItemImage = view.findViewById(R.id.category_image);
            mItemText = view.findViewById(R.id.category_text);
            mSubmenu = view.findViewById(R.id.submenu);
            view.setOnClickListener(this);
        }

        /**
         * Open or close the submenu.
         */
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            MenuListItem clickedItem = mMenuList.get(position);
            if (clickedItem.isExpanded()) {
                clickedItem.setExpanded(false);
            } else {
                // Close all other submenu
                for (MenuListItem item : mMenuList) {
                    if (item.isExpanded()) {
                        item.setExpanded(false);
                    }
                }
                // Open the clicked item's submenu
                clickedItem.setExpanded(true);
            }
            notifyDataSetChanged();
        }
    }

    /**
     * @param parent   of the ViewHolder
     * @param viewType 0 if it doesn't have submenu and 1 if it has.
     * @return ViewHolder
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder;
        switch (viewType) {
            case 0:
                inflatedView = inflater.inflate(R.layout.menu_list_item, parent, false);
                viewHolder = new CategoryHolder(inflatedView);
                break;
            case 1:
                inflatedView = inflater.inflate(R.layout.submenu_list_item, parent, false);
                viewHolder = new SubmenuHolder(inflatedView);
                break;
            default:
                Log.e(TAG, "Impossible viewType!");
                viewHolder = null;
        }
        return viewHolder;
    }

    /**
     * Set the image and text of the given ViewHolder. If it has a submenu, create it too.
     *
     * @param holder   the ViewHolder
     * @param position of the ViewHolder
     */
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        // The object that has the data of the item
        MenuListItem item = mMenuList.get(position);
        switch (holder.getItemViewType()) {
            case 0:
                CategoryHolder categoryHolder = (CategoryHolder) holder;
                // Set the item's image and text
                categoryHolder.mItemText.setText(item.getText());
                categoryHolder.mItemImage.setImageResource(item.getImageResourceId());
                break;
            // If the item has a submenu, create the submenu list
            case 1:
                SubmenuHolder submenuHolder = (SubmenuHolder) holder;
                // Set the item's image and text
                submenuHolder.mItemText.setText(item.getText());
                submenuHolder.mItemImage.setImageResource(item.getImageResourceId());
                // Create the submenu
                LinearLayoutManager manager =
                        new LinearLayoutManager(holder.itemView.getContext());
                RecyclerView submenu = submenuHolder.mSubmenu;
                submenu.setLayoutManager(manager);
                DividerItemDecoration divider = new DividerItemDecoration(
                        holder.itemView.getContext(),
                        manager.getOrientation());
                submenuHolder.mSubmenu.addItemDecoration(divider);
                SubmenuAdapter adapter =
                        new SubmenuAdapter(mMenuList.get(position).getSubmenuList());
                submenu.setAdapter(adapter);
                // Show the submenu if it's expanded
                if (item.isExpanded()) {
                    submenuHolder.mSubmenu.setVisibility(View.VISIBLE);
                } else {
                    submenuHolder.mSubmenu.setVisibility(View.GONE);
                }
                break;
            default:
                Log.e(TAG, "Impossible viewType!");
        }
    }

    /**
     * @return the number of items
     */
    @Override
    public int getItemCount() {
        return mMenuList.size();
    }

    /**
     * @param position of the ViewHolder
     * @return 1 if the item has a submenu and 0 if it doesn't.
     */
    @Override
    public int getItemViewType(int position) {
        if (mMenuList.get(position).hasSubmenu()) {
            return 1;
        } else {
            return 0;
        }
    }
}