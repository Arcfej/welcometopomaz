package com.example.android.welcometopomaz.objects;

import java.util.ArrayList;

/**
 * This object represents a Menu item. It has a text, a picture, and may, or may not has a submenu
 */

public class MenuListItem {

    private String mText;
    private int mImageResourceId;
    private boolean mHasSubmenu;
    // The texts of the submenu items
    private ArrayList<String> mSubmenuList = null;
    // True if the submenu is expanded (visible)
    private boolean mIsExpanded = false;

    /**
     * The constructor of the MenuItem if it doesn't have a submenu.
     *
     * @param text            to display in the list
     * @param imageResourceId of the image to display in the list
     */
    public MenuListItem(String text, int imageResourceId) {
        mText = text;
        mImageResourceId = imageResourceId;
    }

    /**
     * The constructor of the object if it has a submenu.
     *
     * @param text            to display in the list
     * @param imageResourceId of the image to display in the list
     * @param submenuList     is the list of Strings of the submenu
     */
    public MenuListItem(String text, int imageResourceId, ArrayList<String> submenuList) {
        mText = text;
        mImageResourceId = imageResourceId;
        mHasSubmenu = true;
        mSubmenuList = submenuList;
    }

    public String getText() {
        return mText;
    }

    public int getImageResourceId() {
        return mImageResourceId;
    }

    public boolean hasSubmenu() {
        return mHasSubmenu;
    }

    public ArrayList<String> getSubmenuList() {
        return mSubmenuList;
    }

    /**
     * @return true if the submenu is expanded (visible)
     */
    public boolean isExpanded() {
        return mIsExpanded;
    }

    /**
     * Set the submenu expanded or closed
     */
    public void setExpanded(boolean isExpanded) {
        mIsExpanded = isExpanded;
    }
}
